const express = require("express");
const mongoose = require("mongoose");
const users = require("./app/users");
const artists = require('./app/artists');
const albums = require('./app/albums');
const tracks = require('./app/tracks');
const histories = require('./app/histories');
const cors = require('cors');
const exitHook = require('async-exit-hook');
const config = require('./config');


const app = express();

app.use(cors());
app.use(express.json());
app.use(express.static('public'));

const port = 8000;

app.use("/artists", artists);
app.use("/albums", albums);
app.use("/tracks", tracks)
app.use("/users", users);
app.use("/histories", histories)

const run = async () => {
    await mongoose.connect(config.db.url, config.db.options);

    app.listen(port, () => {
        console.log(`Server started on ${port} port!`);
    });

    exitHook(async callback => {
        await mongoose.disconnect();
        console.log('mongoose disconnected');
        callback();
    });
};

run().catch(console.error);