const mongoose = require("mongoose");

const TrackSchema = new mongoose.Schema({
    user: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "User",
        required: true
    },
    name: {
        type: String,
        required: true,
        unique: true
    },
    album: {
        type: mongoose.Schema.Types.ObjectId,
        ref: "Album",
        required: true,
    },
    duration: {
        type: String,
    },
    number: {
        type: Number,
        required: true
    },
    published: {
        type: Boolean,
        required: true,
        default: false
    },
    youtube: {
        type: String,
    }
});

const Track = mongoose.model('Track', TrackSchema);
module.exports = Track;
